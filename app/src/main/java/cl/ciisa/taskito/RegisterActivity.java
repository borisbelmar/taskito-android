package cl.ciisa.taskito;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Arrays;

import cl.ciisa.taskito.controller.AuthController;
import cl.ciisa.taskito.lib.InputValidationManager;
import cl.ciisa.taskito.lib.TextValidator;
import cl.ciisa.taskito.model.user.User;

public class RegisterActivity extends AppCompatActivity {
    private TextInputLayout tilEmail, tilFirstName, tilLastName, tilPassword;
    private Button btnLogin, btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        tilEmail = findViewById(R.id.activity_register_field_email);
        tilFirstName = findViewById(R.id.activity_register_field_first_name);
        tilLastName = findViewById(R.id.activity_register_field_last_name);
        tilPassword = findViewById(R.id.activity_register_field_password);
        btnLogin = findViewById(R.id.activity_register_btn_login);
        btnRegister = findViewById(R.id.activity_register_btn_register);

        InputValidationManager validationManager = new InputValidationManager();

        validationManager.addInputValidation(tilEmail, text -> new TextValidator(text).isRequired().isEmail());
        validationManager.addInputValidation(tilFirstName, text -> new TextValidator(text).isRequired());
        validationManager.addInputValidation(tilLastName, text -> new TextValidator(text).isRequired());
        validationManager.addInputValidation(tilPassword, text -> new TextValidator(text).isRequired());

        btnRegister.setOnClickListener(view -> {
            if (!validationManager.isValid()) {
                Toast.makeText(this, "Campos inválidos", Toast.LENGTH_SHORT).show();
                return;
            }

            String email = tilEmail.getEditText().getText().toString(),
                    firstName = tilFirstName.getEditText().getText().toString(),
                    lastName = tilLastName.getEditText().getText().toString(),
                    password = tilPassword.getEditText().getText().toString();

            User user = new User(firstName, lastName, email);
            user.setPassword(password);

            AuthController controller = new AuthController(view.getContext());

            try {
                controller.register(user);
            } catch (Error error) {
                error.printStackTrace();
                Toast.makeText(view.getContext(), "Hubo un error en el registro", Toast.LENGTH_LONG).show();
            }
        });

        btnLogin.setOnClickListener(view -> {
           super.onBackPressed();
        });
    }
}