package cl.ciisa.taskito;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.Toast;

import cl.ciisa.taskito.controller.AuthController;
import cl.ciisa.taskito.model.user.User;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final int TIMEOUT = 2000;

        AuthController authController = new AuthController(this);

        User user = authController.getUserSession();

        Intent i = new Intent(getBaseContext(), user.getId() == 0 ? LoginActivity.class : MainActivity.class);

        new Handler().postDelayed(() -> {
            if (user.getId() != 0) {
                Toast.makeText(this, String.format("Bienvenido denuevo %s", user.getFullName()), Toast.LENGTH_LONG).show();
            }
            startActivity(i);
            finish();
        }, TIMEOUT);

    }
}