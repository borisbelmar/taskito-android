package cl.ciisa.taskito.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import cl.ciisa.taskito.model.task.TaskEntity;

@Dao
public interface TaskDao {
    @Query("SELECT id, title, description, due, done, user_id FROM tasks WHERE user_id = :userId")
    List<TaskEntity> findAllByUser(long userId);

    @Query("SELECT id, title, description, due, done, user_id FROM tasks WHERE id = :id LIMIT 1")
    TaskEntity findById (long id);

    @Insert
    long insert(TaskEntity task);

    @Update
    void update(TaskEntity task);

    @Delete
    void delete(TaskEntity task);
}
