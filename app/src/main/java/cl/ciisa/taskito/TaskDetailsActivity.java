package cl.ciisa.taskito;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import cl.ciisa.taskito.controller.TaskController;
import cl.ciisa.taskito.model.task.Task;

public class TaskDetailsActivity extends AppCompatActivity {
    TextView tvTitle, tvDescription, tvDue, tvCreatedAt;
    Button btnToggleComplete, btnEdit, btnDelete;
    TaskController taskController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);

        Task task = (Task) getIntent().getSerializableExtra("task");

        taskController = new TaskController(this);

        tvTitle = findViewById(R.id.activity_task_details_tv_title_value);
        tvDescription = findViewById(R.id.activity_task_details_tv_description_value);
        tvDue = findViewById(R.id.activity_task_details_tv_due_value);
        tvCreatedAt = findViewById(R.id.activity_task_details_tv_created_at_value);
        btnToggleComplete = findViewById(R.id.activity_task_details_btn_toggle_complete);
        btnEdit = findViewById(R.id.activity_task_details_btn_edit);
        btnDelete = findViewById(R.id.activity_task_details_btn_delete);

        btnToggleComplete.setText(task.isDone() ? "Marcar como no completado" : "Completar");
        btnToggleComplete.getBackground().setTint(task.isDone() ? Color.RED : Color.GREEN);

        tvTitle.setText(task.getTitle());
        tvDescription.setText(task.getDescription());
        tvDue.setText(task.getStringDue());
        tvCreatedAt.setText(task.getStringCreatedAt());

        btnToggleComplete.setOnClickListener(view -> {
            taskController.toggleComplete(task);
        });

        btnEdit.setOnClickListener(view -> {
            Intent i = new Intent(view.getContext(), TaskEditActivity.class);
            i.putExtra("task", task);
            startActivity(i);
        });

        btnDelete.setOnClickListener(view -> {
            taskController.delete(task);
        });
    }
}