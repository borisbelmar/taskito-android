package cl.ciisa.taskito;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import cl.ciisa.taskito.controller.AuthController;
import cl.ciisa.taskito.controller.TaskController;
import cl.ciisa.taskito.lib.InputValidationManager;
import cl.ciisa.taskito.lib.TextValidator;
import cl.ciisa.taskito.model.task.Task;
import cl.ciisa.taskito.ui.DatePickerFragment;

public class TaskCreateActivity extends AppCompatActivity {
    TextInputLayout tilTitle, tilDescription, tilDue;
    Button btnCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_create);

        tilTitle = findViewById(R.id.activity_task_create_field_title);
        tilDescription = findViewById(R.id.activity_task_create_field_description);
        tilDue = findViewById(R.id.activity_task_create_field_due);
        btnCreate = findViewById(R.id.activity_task_create_btn_create);

        InputValidationManager inputValidationManager = new InputValidationManager();

        inputValidationManager.addInputValidation(tilTitle, text -> new TextValidator(text).isRequired());

        tilDue.getEditText().setOnClickListener(view -> DatePickerFragment.showDatePickerDialog(this, tilDue));

        btnCreate.setOnClickListener(view -> {
            if (!inputValidationManager.isValid()) {
                Toast.makeText(this, "Campos inválidos", Toast.LENGTH_SHORT).show();
                return;
            }

            AuthController authController = new AuthController(view.getContext());
            TaskController controller = new TaskController(view.getContext());

            long userId = authController.getUserSession().getId();
            String title = tilTitle.getEditText().getText().toString();
            String description = tilDescription.getEditText().getText().toString();
            String due = tilDue.getEditText().getText().toString();

            Task task = new Task(
                    title,
                    description,
                    userId
            );

            if (!due.isEmpty()) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    task.setDue(formatter.parse(due));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            controller.create(task);
        });
    }
}