package cl.ciisa.taskito;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;

import cl.ciisa.taskito.controller.AuthController;
import cl.ciisa.taskito.controller.TaskController;

public class MainActivity extends AppCompatActivity {
    private TaskController taskController;
    private AuthController authController;
    private Button btnLogout, btnNewTask;
    private ListView lvTasks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        authController = new AuthController(this);
        taskController = new TaskController(this);

        btnNewTask = findViewById(R.id.activity_main_btn_new_task);
        btnLogout = findViewById(R.id.activity_main_btn_logout);
        lvTasks = findViewById(R.id.activity_main_lv_tasks);

        taskController.renderAllTasks(lvTasks);

        btnNewTask.setOnClickListener(view -> {
            Intent i = new Intent(view.getContext(), TaskCreateActivity.class);
            startActivity(i);
        });

        btnLogout.setOnClickListener(view -> {
            authController.logout();
        });
    }
}