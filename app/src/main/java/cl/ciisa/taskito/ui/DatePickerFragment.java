package cl.ciisa.taskito.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import android.app.DatePickerDialog.OnDateSetListener;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment {
    private OnDateSetListener listener;

    public static DatePickerFragment newInstance(OnDateSetListener listener) {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setListener(listener);
        return fragment;
    }

    public void setListener(OnDateSetListener listener) {
        this.listener = listener;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), listener, year, month, day);
    }

    public static void showDatePickerDialog(AppCompatActivity activity, final TextInputLayout til) {
        DatePickerFragment newFragment = DatePickerFragment.newInstance((datePicker, year, month, day) -> {
            final String selectedDate = String.format("%d-%02d-%02d", year, (month +1), day);
            til.getEditText().setText(selectedDate);
        });
        newFragment.show(activity.getSupportFragmentManager(), "datePicker");
    }
}

