package cl.ciisa.taskito.ui;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cl.ciisa.taskito.R;
import cl.ciisa.taskito.model.task.Task;

public class TaskAdapter extends BaseAdapter {
    private Context ctx;
    private List<Task> taskList;

    public TaskAdapter(Context ctx, List<Task> taskList) {
        this.ctx = ctx;
        this.taskList = taskList;
    }


    @Override
    public int getCount() {
        return this.taskList.size();
    }

    @Override
    public Task getItem(int i) {
        return this.taskList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.taskList.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = LayoutInflater.from(this.ctx);

        view = inflater.inflate(R.layout.item_task, null);

        Task task = this.taskList.get(i);

        TextView tvTitle = view.findViewById(R.id.item_task_title);
        TextView tvDescription = view.findViewById(R.id.item_task_description);
        TextView tvStatus = view.findViewById(R.id.item_task_status);

        tvTitle.setText(task.getTitle());
        tvDescription.setText(task.getDescription());
        tvStatus.setText(task.isDone() ? "Terminado" : "Pendiente");
        tvStatus.setTextColor(task.isDone() ? Color.GREEN : Color.RED);

        return view;
    }
}
