package cl.ciisa.taskito.model.task;

public class TaskMapper {
    private final ITask task;

    public TaskMapper(ITask task) {
        this.task = task;
    }

    public TaskEntity toEntity() {
        return new TaskEntity(
            this.task.getId(),
            this.task.getTitle(),
            this.task.getDue(),
            this.task.getDescription(),
            this.task.isDone(),
            this.task.getUserId(),
            this.task.getCreatedAt()
        );
    }

    public Task toBase() {
        Task baseTask =  new Task(task.getTitle(), task.getDescription(), task.getUserId());
        baseTask.setId(task.getId());
        baseTask.setDue(task.getDue());
        baseTask.setDone(task.isDone());
        return baseTask;
    }
}
