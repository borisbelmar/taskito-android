package cl.ciisa.taskito.model.task;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cl.ciisa.taskito.model.user.IUser;
import cl.ciisa.taskito.model.user.User;

public class Task implements ITask, Serializable {
    private long id;
    private String title;
    private Date due;
    private String description;
    private boolean done;
    private long userId;
    private Date createdAt;

    public Task(String title, String description, long userId) {
        this.title = title;
        this.description = description;
        this.userId = userId;
        this.createdAt = new Date();
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDue(Date due) {
        this.due = due;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public Date getDue() {
        return due;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean isDone() {
        return done;
    }

    @Override
    public long getUserId() {
        return userId;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    public String getStringDue() {
        if (due == null) {
            return "";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(due);
    }

    public String getStringCreatedAt() {
        if (createdAt == null) {
            return "";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss", Locale.getDefault());
        return dateFormat.format(createdAt);
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", due=" + due +
                ", description='" + description + '\'' +
                ", done=" + done +
                ", userId=" + userId +
                '}';
    }
}
