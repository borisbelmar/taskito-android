package cl.ciisa.taskito.model.user;

public interface IUser {
    long getId();
    String getFirstName();
    String getLastName();
    String getEmail();
    String getPassword();
}