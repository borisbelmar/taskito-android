package cl.ciisa.taskito.model.task;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

import cl.ciisa.taskito.model.user.UserEntity;


@Entity(tableName = "tasks")
public class TaskEntity implements ITask {
    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "due")
    private Date due;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "done")
    private boolean done;

    @ColumnInfo(name = "user_id")
    private long userId;

    @ColumnInfo(name = "created_at")
    private Date createdAt;

    public TaskEntity(long id, String title, Date due, String description, boolean done, long userId, Date createdAt) {
        this.id = id;
        this.title = title;
        this.due = due;
        this.description = description;
        this.done = done;
        this.userId = userId;
        this.createdAt = createdAt;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public Date getDue() {
        return due;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean isDone() {
        return done;
    }

    @Override
    public long getUserId() {
        return userId;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }
}
