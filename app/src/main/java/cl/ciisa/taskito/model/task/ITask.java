package cl.ciisa.taskito.model.task;

import java.util.Date;

public interface ITask {
    long getId();
    String getTitle();
    Date getDue();
    String getDescription();
    boolean isDone();
    Date getCreatedAt();
    long getUserId();
}
