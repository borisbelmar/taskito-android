package cl.ciisa.taskito.model.user;

public class UserMapper {
    private IUser user;

    public UserMapper(IUser user) {
        this.user = user;
    }

    public UserEntity toEntity() {
        return new UserEntity(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPassword()
        );
    }

    public User toBase() {
        User baseUser = new User(user.getFirstName(), user.getLastName(), user.getEmail());
        baseUser.setId(user.getId());
        baseUser.setPassword(user.getPassword());
        return baseUser;
    }
}
