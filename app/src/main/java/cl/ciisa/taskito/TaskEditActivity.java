package cl.ciisa.taskito;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import cl.ciisa.taskito.controller.AuthController;
import cl.ciisa.taskito.controller.TaskController;
import cl.ciisa.taskito.lib.InputValidationManager;
import cl.ciisa.taskito.lib.TextValidator;
import cl.ciisa.taskito.model.task.Task;
import cl.ciisa.taskito.ui.DatePickerFragment;

public class TaskEditActivity extends AppCompatActivity {
    TextInputLayout tilTitle, tilDescription, tilDue;
    Button btnEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_edit);

        Task task = (Task) getIntent().getSerializableExtra("task");

        tilTitle = findViewById(R.id.activity_task_edit_field_title);
        tilDescription = findViewById(R.id.activity_task_edit_field_description);
        tilDue = findViewById(R.id.activity_task_edit_field_due);
        btnEdit = findViewById(R.id.activity_task_edit_btn_edit);

        tilTitle.getEditText().setText(task.getTitle());
        tilDescription.getEditText().setText(task.getDescription());
        tilDue.getEditText().setText(task.getStringDue());

        InputValidationManager inputValidationManager = new InputValidationManager();

        inputValidationManager.addInputValidation(tilTitle, text -> new TextValidator(text).isRequired());

        tilDue.getEditText().setOnClickListener(view -> DatePickerFragment.showDatePickerDialog(this, tilDue));

        btnEdit.setOnClickListener(view -> {
            if (!inputValidationManager.isValid()) {
                Toast.makeText(this, "Campos inválidos", Toast.LENGTH_SHORT).show();
                return;
            }

            AuthController authController = new AuthController(view.getContext());
            TaskController controller = new TaskController(view.getContext());

            long userId = authController.getUserSession().getId();
            String title = tilTitle.getEditText().getText().toString();
            String description = tilDescription.getEditText().getText().toString();
            String due = tilDue.getEditText().getText().toString();

            Task updateTask = new Task(
                    title,
                    description,
                    userId
            );

            updateTask.setId(task.getId());
            updateTask.setCreatedAt(task.getCreatedAt());

            if (!due.isEmpty()) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    updateTask.setDue(formatter.parse(due));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            controller.update(updateTask);
        });
    }
}