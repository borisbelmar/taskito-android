package cl.ciisa.taskito.controller;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cl.ciisa.taskito.MainActivity;
import cl.ciisa.taskito.TaskDetailsActivity;
import cl.ciisa.taskito.dao.TaskDao;
import cl.ciisa.taskito.lib.TaskitoDatabase;
import cl.ciisa.taskito.model.task.Task;
import cl.ciisa.taskito.model.task.TaskEntity;
import cl.ciisa.taskito.model.task.TaskMapper;
import cl.ciisa.taskito.ui.TaskAdapter;

public class TaskController {
    private Context ctx;
    private TaskDao taskDao;

    public TaskController(Context ctx) {
        this.ctx = ctx;
        this.taskDao = TaskitoDatabase.getInstance(ctx).taskDao();
    }

    private void redirectToMainActivity() {
        Intent i = new Intent(ctx, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        ctx.startActivity(i);
    }

    public void create(Task task) {
        TaskMapper mapper = new TaskMapper(task);
        TaskEntity taskEntity = mapper.toEntity();

        taskDao.insert(taskEntity);

        this.redirectToMainActivity();
    }

    public void renderAllTasks(ListView listView) {
        AuthController authController = new AuthController(this.ctx);
        long userId = authController.getUserSession().getId();

        List<TaskEntity> taskEntityList = taskDao.findAllByUser(userId);

        List<Task> taskList = new ArrayList<>();;

        for (TaskEntity taskEntity : taskEntityList) {
            TaskMapper mapper = new TaskMapper(taskEntity);
            taskList.add(mapper.toBase());
        }

        TaskAdapter adapter = new TaskAdapter(this.ctx, taskList);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener((adapterView, view, pos, id) -> {
            Task task = taskList.get(pos);

            Intent i = new Intent(view.getContext(), TaskDetailsActivity.class);
            i.putExtra("task", task);
            view.getContext().startActivity(i);
        });
    }

    public void toggleComplete(Task task) {
        task.setDone(!task.isDone());
        TaskMapper mapper = new TaskMapper(task);
        taskDao.update(mapper.toEntity());

        this.redirectToMainActivity();
    }

    public void update(Task task) {
        TaskMapper mapper = new TaskMapper(task);
        taskDao.update(mapper.toEntity());
        Toast.makeText(this.ctx, "Se actualizó la tarea con éxito", Toast.LENGTH_SHORT).show();
        this.redirectToMainActivity();
    }

    public void delete(Task task) {
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    try {
                        TaskMapper mapper = new TaskMapper(task);
                        taskDao.delete(mapper.toEntity());
                        Toast.makeText(this.ctx, "Se eliminó con éxito", Toast.LENGTH_SHORT).show();
                        this.redirectToMainActivity();
                    } catch(Error error) {
                        error.printStackTrace();
                        Toast.makeText(this.ctx, "Error al eliminar la tarea", Toast.LENGTH_SHORT).show();
                    }

                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this.ctx);
        builder.setMessage(String.format("Estás seguro de eliminar la tarea %s?", task.getTitle()))
                .setPositiveButton("Si", dialogClickListener)
                .setNegativeButton("No", dialogClickListener)
                .show();
    }
}
