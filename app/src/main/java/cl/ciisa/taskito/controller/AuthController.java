package cl.ciisa.taskito.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import cl.ciisa.taskito.LoginActivity;
import cl.ciisa.taskito.MainActivity;
import cl.ciisa.taskito.dao.UserDao;
import cl.ciisa.taskito.lib.BCrypt;
import cl.ciisa.taskito.lib.TaskitoDatabase;
import cl.ciisa.taskito.model.user.IUser;
import cl.ciisa.taskito.model.user.User;
import cl.ciisa.taskito.model.user.UserEntity;
import cl.ciisa.taskito.model.user.UserMapper;

public class AuthController {
    private final String KEY_USER_ID = "userId";
    private final String KEY_EMAIL = "userEmail";
    private final String KEY_FIRST_NAME = "userFirstName";
    private final String KEY_LAST_NAME = "userLastName";

    private Context ctx;
    private SharedPreferences pref;

    public AuthController(Context ctx) {
        this.ctx = ctx;
        int PRIVATE_MODE = 0;
        String PREF_NAME = "TaskitoPref";
        this.pref = ctx.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }

    public User getUserSession() {
        long id = pref.getLong(KEY_USER_ID, 0);
        String firstName = pref.getString(KEY_FIRST_NAME, "");
        String lastName = pref.getString(KEY_LAST_NAME, "");
        String email = pref.getString(KEY_EMAIL, "");

        User user = new User(firstName, lastName, email);
        user.setId(id);

        return user;
    }

    public void setUserSession(IUser user) {
        SharedPreferences.Editor editor = this.pref.edit();
        editor.putLong(KEY_USER_ID, user.getId());
        editor.putString(KEY_EMAIL, user.getEmail());
        editor.putString(KEY_FIRST_NAME, user.getFirstName());
        editor.putString(KEY_LAST_NAME, user.getLastName());
        editor.apply();
    }

    public void login(String email, String password) {
        UserDao dao = TaskitoDatabase.getInstance(ctx).userDao();
        UserEntity user = dao.findByEmail(email);

        if (user == null || !BCrypt.checkpw(password, user.getPassword())) {
            Toast.makeText(ctx, "Las credenciales son incorrectas", Toast.LENGTH_SHORT).show();
            return;
        }

        UserMapper mapper = new UserMapper(user);

        User baseUser = mapper.toBase();

        setUserSession(baseUser);

        Toast.makeText(ctx, String.format("Bienvenido %s", baseUser.getFullName()), Toast.LENGTH_LONG).show();

        Intent i = new Intent(ctx, MainActivity.class);
        ctx.startActivity(i);
        ((Activity) ctx).finish();
    }

    public void register(User user) {
        UserDao dao = TaskitoDatabase.getInstance(ctx).userDao();

        String hashedPw = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());

        user.setPassword(hashedPw);

        UserMapper mapper = new UserMapper(user);

        dao.insert(mapper.toEntity());

        Toast.makeText(ctx, "Cuenta registrada con éxito", Toast.LENGTH_SHORT).show();

        Intent i = new Intent(ctx, LoginActivity.class);
        ctx.startActivity(i);
        ((Activity) ctx).finish();
    }

    public void logout() {
        SharedPreferences.Editor editor = this.pref.edit();
        editor.clear();
        editor.apply();
        Intent i = new Intent(this.ctx, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.ctx.startActivity(i);
    }
}
