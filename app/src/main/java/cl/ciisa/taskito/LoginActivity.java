package cl.ciisa.taskito;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import cl.ciisa.taskito.controller.AuthController;

public class LoginActivity extends AppCompatActivity {
    private TextInputLayout tilEmail, tilPassword;
    private Button btnLogin, btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tilEmail = findViewById(R.id.activity_login_field_email);
        tilPassword = findViewById(R.id.activity_login_field_password);
        btnLogin = findViewById(R.id.activity_login_btn_login);
        btnRegister = findViewById(R.id.activity_login_btn_register);

        btnLogin.setOnClickListener(view -> {
            AuthController controller = new AuthController(view.getContext());

            String email = tilEmail.getEditText().getText().toString(),
                    password = tilPassword.getEditText().getText().toString();

            try {
                controller.login(email, password);
            } catch (Error error) {
                error.printStackTrace();
                Toast.makeText(view.getContext(), "Hubo un error en el inicio de sesión", Toast.LENGTH_LONG).show();
            }
        });

        btnRegister.setOnClickListener(view -> {
            Intent i = new Intent(view.getContext(), RegisterActivity.class);
            startActivity(i);
        });
    }
}