package cl.ciisa.taskito.lib;

import android.text.Editable;
import android.text.TextWatcher;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

public class InputValidationManager {
    List<InputValidation> validations = new ArrayList<>();

    public void addInputValidation(TextInputLayout til, TextChangeListener listener) {
        this.validations.add(new InputValidation(til, listener));
    }

    public boolean isValid() {
        for (InputValidation validation : validations) {
            if (!validation.isValid()) { return false; }
        }
        return true;
    }

    public interface TextChangeListener {
        TextValidator onTextChange(String text);
    }

    private class InputValidation {
        private final TextInputLayout til;
        private final TextChangeListener listener;
        private TextValidator textValidator;

        public InputValidation(TextInputLayout til, TextChangeListener listener) {
            this.til = til;
            this.listener = listener;
            this.textValidator = listener.onTextChange(til.getEditText() != null ? til.getEditText().getText().toString() : "");
            this.listen();
        }

        private void listen() {
            if (til.getEditText() != null) {
                til.getEditText().addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        textValidator = listener.onTextChange(charSequence.toString());
                        if(textValidator.isValid()) {
                            til.setError(null);
                            til.setErrorEnabled(false);
                        } else {
                            til.setError(textValidator.getErrorMessage());
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
            }
        }

        public boolean isValid() {
            return textValidator.isValid();
        }
    }
}
