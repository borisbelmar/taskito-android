package cl.ciisa.taskito.lib;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import cl.ciisa.taskito.dao.TaskDao;
import cl.ciisa.taskito.dao.UserDao;
import cl.ciisa.taskito.model.task.TaskEntity;
import cl.ciisa.taskito.model.user.UserEntity;
import cl.ciisa.taskito.utils.Converters;

@Database(entities = {UserEntity.class, TaskEntity.class}, version = 3)
@TypeConverters({Converters.class})
public abstract class TaskitoDatabase extends RoomDatabase {
    private static final String DB_NAME = "taskito_db";
    private static TaskitoDatabase instance;

    public static synchronized TaskitoDatabase getInstance(Context ctx) {
        if (instance == null) {
            instance = Room.databaseBuilder(ctx.getApplicationContext(), TaskitoDatabase.class, DB_NAME)
                    .allowMainThreadQueries() // TODO: Código bloqueante, solo para explicar mejor, pero es mala práctica!
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract UserDao userDao();
    public abstract TaskDao taskDao();
}
