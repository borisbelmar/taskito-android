package cl.ciisa.taskito.lib;

import android.text.TextUtils;
import android.util.Patterns;

public class TextValidator {
    private String text;
    private boolean valid;
    private boolean empty;
    private String errorMessage;

    public TextValidator(String text) {
        this.text = text;
        this.valid = true;
        this.empty = TextUtils.isEmpty(text);
    }

    public TextValidator isRequired() {
        this.valid = !this.empty;
        this.errorMessage = !this.valid ? "Campo requerido" : null;
        return this;
    }

    public TextValidator isEmail() {
        if (!this.empty) {
            this.valid = Patterns.EMAIL_ADDRESS.matcher(this.text).matches();
            this.errorMessage = !this.valid ? "Email inválido" : null;
        }
        return this;
    }

    public boolean isValid() {
        return this.valid;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }
}
